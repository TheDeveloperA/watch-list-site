﻿namespace Watch_List_Site.Models
{
    public class WatchList
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string PosterUrl { get; set; }
        public string Desc { get; set; }
        public string UserId { get; set; }

    }
}
