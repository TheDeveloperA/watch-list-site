﻿using System.Collections.Generic;

namespace Watch_List_Site.Models
{
    public class TmdbResponse
    {
        public IEnumerable<SearchEntry> results { get; set; }
    }
}
