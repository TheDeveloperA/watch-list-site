﻿namespace Watch_List_Site.Models
{
    public class SearchEntry
    {
        public int Id { get; set; }
        public string Original_Name { get; set; }
        public string Poster_Path { get; set; }
        public string Overview { get; set; }
    }
}
