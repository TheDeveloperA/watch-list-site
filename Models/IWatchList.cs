﻿using System.Collections.Generic;

namespace Watch_List_Site.Models
{
    public interface IWatchList
    {
        void AddItem(WatchList watchList);
        void RemoveItem(int id);
        WatchList GetItemByID(int id);
        IEnumerable<WatchList> GetAllByUserId(string userId);
        void SaveToDb();
    }
}
