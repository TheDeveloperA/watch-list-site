﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Watch_List_Site.Models;

namespace Watch_List_Site.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public DbSet<WatchList> WatchList { get; set; }
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
    }
}
