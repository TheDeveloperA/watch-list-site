﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Watch_List_Site.Models;

namespace Watch_List_Site.Controllers
{
    public class HomeController : Controller
    {
        private readonly UserManager<IdentityUser> _userManager;
        private IWatchList _watchListRepo;

        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger, UserManager<IdentityUser> userManager, IWatchList watchList)
        {
            _logger = logger;
            _userManager = userManager;
            _watchListRepo = watchList;
        }

        public async Task<IActionResult> Index()
        {
            IdentityUser user = await _userManager.GetUserAsync(User);

            IEnumerable<WatchList> model = null;
            if (user != null)
            {
                model = _watchListRepo.GetAllByUserId(user.Id);
            }
            
            return View(model);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public Task<IActionResult> GetSearch(string query)
        {

            var searchList = SearchAPI(query);

            return searchList;
        }

        public async Task<IActionResult> SearchAPI(string query)
        {
            using (var client = new HttpClient())
            {
                try
                {
                    client.BaseAddress = new Uri("https://api.themoviedb.org");
                    var response = await client.GetAsync($"/3/search/tv?api_key=a4849f30af06ceaee8" +
                        $"cc951ab045347e&language=en-US&page=1&query={query}&include_adult=false");

                    response.EnsureSuccessStatusCode();

                    var stringResult = await response.Content.ReadAsStringAsync();
                    var rawResult = JsonConvert.DeserializeObject<TmdbResponse>(stringResult);

                    return Ok(new
                    {
                        Id = rawResult.results.Select(x => x.Id),
                        Name = rawResult.results.Select(x => x.Original_Name),
                        Desc = rawResult.results.Select(x => x.Overview),
                        Image = rawResult.results.Select(x => x.Poster_Path)
                    }); 
                }
                catch(HttpRequestException httpRequestException)
                {
                    return BadRequest($"Error getting Data from Api:{httpRequestException.Message}");
                }
            }
        }
    }
}
