﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Watch_List_Site.Models;

namespace Watch_List_Site.Controllers
{
    public class DetailsController : Controller
    {
        private readonly UserManager<IdentityUser> _userManager;
        private IWatchList _watchListRepo;
        public DetailsController(UserManager<IdentityUser> userManager,IWatchList watchList)
        {
            _userManager = userManager;
            _watchListRepo = watchList;
        }
        public IActionResult Index(int id, string name, string desc, string image, bool isSaved)
        {
            SearchEntry searchEntry = null;

            if (isSaved)
            {
                ViewBag.isSaved = true;
                searchEntry = new SearchEntry
                {
                    Id = id,
                    Original_Name = name,
                    Overview = desc,
                    Poster_Path = image
                };
            }
            else { 
                ViewBag.isSaved = false;
                searchEntry = new SearchEntry
                {
                    Id = id,
                    Original_Name = name,
                    Overview = desc,
                    Poster_Path = "http://image.tmdb.org/t/p/w600_and_h900_bestv2" + image
                };
            }


            return View(searchEntry);
        }

        public async Task<IActionResult> AddToWatchList(string name, string desc, string image)
        {

            IdentityUser user = await _userManager.GetUserAsync(User);
            var entry = new WatchList {
                Title = name,
                Desc = desc,
                PosterUrl = image,
                UserId = user.Id
            };

            _watchListRepo.AddItem(entry);
            _watchListRepo.SaveToDb();

            return RedirectToAction("Index", "Home");
        }

        public IActionResult RemoveFromWatchList(int id)
        {
            _watchListRepo.RemoveItem(id);
            _watchListRepo.SaveToDb();

            return RedirectToAction("Index", "Home");
        }

    }
}
