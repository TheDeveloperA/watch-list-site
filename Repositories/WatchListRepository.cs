﻿using System.Collections.Generic;
using System.Linq;
using Watch_List_Site.Data;
using Watch_List_Site.Models;

namespace Watch_List_Site.Repositories
{
    public class WatchListRepository : IWatchList
    {
        private readonly ApplicationDbContext _context;

        public WatchListRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        public void AddItem(WatchList watchList)
        {
            _context.Add(watchList);
        }

        public void RemoveItem(int id)
        {
            var item = GetItemByID(id);
            _context.Remove(item);
        }

        public IEnumerable<WatchList> GetAllByUserId(string userId)
        {
            return _context.WatchList.Where(w => w.UserId == userId);
        }

        public WatchList GetItemByID(int id)
        {
            return _context.WatchList.Where(w => w.Id == id).FirstOrDefault();          
        }

        public void SaveToDb()
        {
            _context.SaveChanges();
        }
    }
}
